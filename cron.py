from database import get_cron_entries


def cron_data():
    cronfile = ""
    for i in get_cron_entries():
        cronfile += f"{i[0]} python3 /opt/backuppro/backup.py {i[1]}\n"
    return cronfile


def update_cron_file():
    file = open("/etc/cron.d/bcpp", "w")
    file.write(cron_data())
    file.close()
