import sqlite3
import os


if os.path.isfile('data.db'):
    conn = sqlite3.connect('data.db')
    c = conn.cursor()
else:
    conn = sqlite3.connect('data.db')
    c = conn.cursor()
    c.execute("""CREATE TABLE "clients" (
                "id"	INTEGER NOT NULL UNIQUE,
                "name"	TEXT NOT NULL UNIQUE,
                "user" TEXT NOT NULL,
                "password"	TEXT NOT NULL,
                "address"	TEXT NOT NULL,
                "port"	INTEGER NOT NULL,
                "srcdir"	TEXT NOT NULL,
                "srvdir"	TEXT NOT NULL,
                "type"	INTEGER NOT NULL,
                "cron"	TEXT NOT NULL,
                PRIMARY KEY("id" AUTOINCREMENT));""")


def client_dict(client_list):
    result = {"id": client_list[0], "name": client_list[1],
              "user": client_list[2],
              "password": client_list[3], "address": client_list[4],
              "port": client_list[5], "srcdir": client_list[6],
              "srvdir": client_list[7]}
    if client_list[8] == 0:
        result.update({"type": "rsync", "cron": client_list[9]})
    else:
        result.update({"type": "scp", "cron": client_list[9]})
    return result


def get_cient_by_name(name):
    c.execute("""SELECT * FROM clients WHERE name = ?""", (name,))
    # print(c.fetchone())
    return client_dict(c.fetchone())


def add_client(client_list):
    c.execute(
        """INSERT INTO clients (name, user, password, address, port, srcdir, srvdir, type, cron) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)""",
        client_list)
    conn.commit()


def get_cron_entries():
    c.execute("""SELECT cron, name FROM clients""")
    result = []
    for i in c.fetchall():
        result.append(i)
    return result

def get_all_clients():
    c.execute("""SELECT * FROM clients""")
    result = []
    for i in c.fetchall():
        result.append(client_dict(i))
    return result
# print(get_cient_by_name("www"))
