import os
import sys
import time

from database import get_cient_by_name


def scp(c):
    timestamp = str(int(time.time()))
    os.system(f'mkdir {c["srvdir"]}/{timestamp}')
    os.system(
        f'sshpass -p "{c["password"]}" scp -r -P {c["port"]} {c["user"]}@{c["address"]}:{c["srcdir"]} {c["srvdir"]}/{timestamp}')


def rsync(c):
    os.system(f'''sshpass -p "{c["password"]}" rsync -r -e 'ssh -p {c["port"]}' {c["user"]}@{c["address"]}:{c["srcdir"]} {c["srvdir"]}/{str(int(time.time()))}''')


args = sys.argv
if len(args) < 1:
    print("Wrong usage. Use name of backup host as only argument.")
    exit()
client = get_cient_by_name(args[1])
if client is None:
    print("Client not found")
    exit()
if client["type"] == "rsync":
    rsync(client)
else:
    scp(client)
