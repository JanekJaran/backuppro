import os

from cron import cron_data
from database import get_all_clients


def show_client(client_dict, hide_pass=True):
    if hide_pass:
        client_dict["password"] = "********"
    for i in client_dict:
        print(f'{i}:{" " * (10 - len(i))}{client_dict[i]}')


def show_client_list():
    print(
        " ID |      name       |    username     |   ip address    | port  |   source directory   |   target directory   | type  |      cron     ")
    for i in get_all_clients():
        j = 1
        print(
            f'{" " * (3 - len(str(i["id"])))}{i["id"]} | {" " * (15 - len(i["name"]))}{i["name"]} | {" " * (15 - len(i["user"]))}{i["user"]} | {" " * (15 - len(i["address"]))}{i["address"]} | {" " * (5 - len(str(i["port"])))}{i["port"]} | {" " * (20 - len(i["srcdir"]))}{i["srcdir"]} | {" " * (20 - len(i["srvdir"]))}{i["srvdir"]} | {" " * (5 - len(i["type"]))}{i["type"]} | {" " * (15 - len(i["cron"]))}{i["cron"]}')
        j += 1
        if j > 15:
            # os.system("pause")
            os.system('read -sn 1 -p " Press any key for next page..."')
            j = 0


# show_client(get_cient_by_name("www"))
def show_cron():
    print(cron_data())
